struct Bitacora{
 char *titulo;
 char *fecha;
 char  *descripcion;
};


void SC_introducirT(struct Bitacora, char*);
void SC_introducirF(struct Bitacora, char*);
void SC_introducirD(struct Bitacora, char*);

char* SC_getTitulo(struct Bitacora);
char* SC_getFecha(struct Bitacora);
char* SC_getDescripcion(struct Bitacora);

void SC_crearBi(struct Bitacora, char*, char*, char*);
void SC_ejecutarBi(struct Bitacora);



