#include"scheduler.h"


void SC_introducirT(struct Bitacora b, char* titulo){
strcpy(b.titulo,titulo);
}

void SC_introducirF(struct Bitacora l, char* fecha){
strcpy(l.fecha,fecha);
}

void SC_introducirD(struct Bitacora p, char* descripcion){
strcpy(p.descripcion,descripcion);
}

char* SC_getTitulo(struct Bitacora f){
return f.titulo;
}

char* SC_getFecha(struct Bitacora n){
return n.fecha;
}
char* SC_getDescripcion(struct Bitacora d){
return d.descripcion;
}

void SC_crearBi(struct Bitacora b, char* titulo, char* fecha, char* descripcion){
 SC_introducirT(b,titulo);
 SC_introducirF(b,fecha);
 SC_introducirD(b,descripcion);
}
void SC_ejecutarBi(struct Bitacora b){
printf("la tarea de nombre %s, se esta ejecutando",SC_getTitulo(b));
printf("la tarea de la fecha %s, se esta ejecutando",SC_getFecha(b));
printf("la tarea que tiene caracteristicas %s, se esta ejecutando",SC_getDescripcion(b));
}
